package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.DemoUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
public interface DemoUserMapper extends BaseMapper<DemoUser> {

    List<DemoUser> findAll();

    void insertUser(DemoUser user);

    void updateUser(String oldName, String nowName, String sex);
}
