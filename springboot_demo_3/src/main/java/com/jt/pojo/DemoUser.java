package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Value;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("demo_user")
public class DemoUser {
    @TableId(type = IdType.AUTO)
    //@TableId(type = IdType.ASSIGN_ID)
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
}
