package com.jt;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.DemoUserMapper;
import com.jt.pojo.DemoUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SpringBootTest

public class TestMP {
    @Autowired
    private DemoUserMapper userMapper;
    @Test
    public void insert(){
        //添加日志的打印
        DemoUser user = new DemoUser();
        user.setAge(12).setName("Mp测试").setSex("男");
        userMapper.insertUser(user);
    }

    @Test
    public void  updateById(){
        DemoUser user = new DemoUser();
        user.setName("中午吃什么").setAge(18).setId(231);
    }
    /**
     * 查询ID=21的用户
     * */
    @Test
    public void testSelect(){
        DemoUser user = userMapper.selectById(21);
        System.out.println(user);
        //2.根据属性查询
        DemoUser user2 = new DemoUser();
        user2.setName("白骨精").setSex("女");
        QueryWrapper<DemoUser> queryWrapper =
                new QueryWrapper<>(user2);
        List<DemoUser> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }
    /**
     * 需求:查询age>18岁 并且性别为女的用户
     *
     * */
    @Test
    public void testSelect2(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.gt("age", 18).eq("sex", "女");
        List<DemoUser> demoUsers = userMapper.selectList(queryWrapper);
        System.out.println(demoUsers);
    }
    /**
     * 查询name中包含"精"字的
     * */
    @Test
    public void testSelect3(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", "精");
        List<DemoUser> demoUsers = userMapper.selectList(queryWrapper);
        System.out.println(demoUsers);
    }
    /**
     * 5.查询id=1.3.5.6的用户
     * */
    @Test
    public void testSelect4(){
        Integer[] ids = {1,3,5,6,7};
        //List<Integer> idList = Arrays.asList(ids);  //根据list查询list集合功能强大
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id",ids);  //数组必须包装类型
        List<DemoUser> demoUsers = userMapper.selectList(queryWrapper);
        System.out.println(demoUsers);
    }
    @Test
    public void testSelect5(){
        List objs = userMapper.selectObjs(null);
        System.out.println(objs);
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();

        List<DemoUser> demoUsers = userMapper.selectList(queryWrapper);
        System.out.println(demoUsers);
    }
    @Test
    public void testSelect6(){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("sex","男");
        List objs = userMapper.selectObjs(queryWrapper);
        System.out.println(objs);
    }
    @Test
    public void testSelect7(){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.select("name","sex");
        List objs = userMapper.selectList(queryWrapper);
        System.out.println(objs);
    }
    @Test
    public void testSelect8(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("name","sex");
        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper);
        System.out.println(maps);
    }
    @Test
    public void testSelect9(){
        QueryWrapper<DemoUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("name","sex");
        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper);
        System.out.println(maps);
    }
    @Test
    public void testSelect10(){
        DemoUser user = new DemoUser();
        user.setName("晚上").setSex("其他");
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name", "中午");
        userMapper.update(user, updateWrapper);
    }
}
