package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class AxiosUserController {

    @Autowired
    private UserService userService;
    @GetMapping("/axiosUser/findAll")
    public List<User> findAll(){
        return userService.findAll();
    }
    @PutMapping("/axiosUser/updateUser")
    public void update(@RequestBody User user){
        userService.updateUser(user);
    }
    @DeleteMapping("/axiosUser/deleteUser")
    public void deleteUser(User user){
        userService.deleteUser(user);
    }
}
