package com.jt.controller;

import com.jt.pojo.AxiosPOJO;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin //允许当前类当中的所有方法执行跨域操作
public class AxiosController {
    /**
     * url:http://localhost:8090/hello
     * */
    @GetMapping("/hello")
    public String hello(){
        return "VUE的Ajax(get)异步调用";
    }
    @GetMapping("/axios")
    public String getAxios(Integer id){
        return "动态数据"+id;
    }
    @GetMapping("/axios/{id}")
    public String getAxiosrestFul(@PathVariable Integer id){
        return "restFul动态数据"+id;
    }
    @GetMapping("/axiosParams")
    public String getParams(){
        return "params传参";
    }
    @PostMapping("/addAxios")
    public String addAxios(@RequestBody AxiosPOJO axiosPOJO){
        return "post请求成功"+axiosPOJO;
    }
    @PostMapping("/addAxiosForm")
    public String addAxiosForm(AxiosPOJO axiosPOJO){
        return "form提交"+axiosPOJO;
    }
}
