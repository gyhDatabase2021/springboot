package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.jws.soap.SOAPBinding;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/demo")
    @ResponseBody
    public String demo(){
        return "框架整合初步完成";
    }

    @RequestMapping("/userList")
    public String userList(Model model){
        List<User> userList = userService.findAll();
        //2.将数据保存到Model对象中返回给页面
        model.addAttribute("userList",userList);
        return "userList";
    }
    /**
     * 需求:利用restFul实现用户数据新增
     *    新增之后重定向到userLIst.html
     * URL地址:/user/唐嫣/12/男
     * */
    @RequestMapping("/user/{name}/{age}/{sex}")
    public String restFuluser(User user){
        userService.insertUser(user);
        return "redirect:/userList";
    }
    @RequestMapping("/userupdate")
    public String update(User user){
        userService.updateUser(user);
        return "redirect:/userList";
    }
    @RequestMapping("/user")
    public String delete(User user){
        userService.deleteUser(user);
        return "redirect:/userList";
    }
    @RequestMapping("/userAjax")
    public String userAjax(){
        return "userAjax";
    }
    @RequestMapping("/findAjaxUser")
    @ResponseBody
    public List<User> findAjaxUser(){
        return userService.findAll();
    }
}
