package com.jt.contorller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller  //将该类交给Spring管理
public class HelloController {
    /**
     * 需求:http://
     * 实现步骤:
     * 1.拦截用户
     * 2.String类型的返回值 表示返回页面服务
     * 3.根据YML配置文件中的内容 动态的拼接前缀后后缀 形成页面的唯一路径
     * */
    @RequestMapping("/hello")
    public String hello(){
        //动态的拼接前缀+后缀
        //classpath:/templates/hello.html
        return "hello";  //有后缀提供支持
    }

}
