package com.jt.contorller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SayController {
    @RequestMapping("/say")
    public String say(){
        return "say";
    }

}
