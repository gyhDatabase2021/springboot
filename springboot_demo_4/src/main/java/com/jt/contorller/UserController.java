package com.jt.contorller;

import com.jt.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Controller
public class UserController {
    //转发时会携带用户的数据
    @RequestMapping("/findUser")
    public String findUser(String name){
        //return "user1";
        //return "dog";如果直接转向到页面中,如果需要额外的参数处理,则没有执行
        //如果在该方法中
        //return "forward:/findDog";\
        /**
         * 特点:
         * 1.转发时 会携带用户提交的数据
         * 2.转发时 用户浏览器的地址不会发生改变
         * 3.重定向时 由于是多次请求,所以不会携带用户数据
         * 4.重定向时 由于是多次请求,所以用户的浏览器的地址会变化
         * */
        return "redirect:/findDog";
    }
    @RequestMapping("/findDog")
    public String findDog(String name, Model model){
        System.out.println("动态获取name属性值:"+name);
        model.addAttribute("name",name+"旺旺");
        return "dog";
    }
    @RequestMapping("/addUser")
    public String addUser(User user){
        System.out.println(user);
        return "success";
    }
    @RequestMapping("/restFul/{id}/{name}")
    public String restFul(@PathVariable Integer id,
                          @PathVariable String name){
        System.out.println("获取参数:"+id+"|"+name);
        return "success";
    }
//    @RequestMapping("/addUser")
//    public String addHobbys(User user){
//
//        System.out.println(user);
//        return "success";
//    }
//    @RequestMapping("/addUser")
//    public String addParam(@RequestParam(value="id") Integer id, String name){
//        System.out.println("参数获取"+id+":"+name);
//        return "success";
//    }

//    @RequestMapping("/addUser")
//    public String addUser(HttpServletRequest request){
//        Integer id = Integer.parseInt(request.getParameter("id"));//接受的的数据都是String类型
//        String name = request.getParameter("name");
//        System.out.println("参数"+id+"姓名"+name);
//        return "success";
//    }

    //简化数据传递
    @RequestMapping("/user")
    public String toUser(Model model){
        model.addAttribute("id",1003);
        model.addAttribute("name","SpringMVC");
        return "user";
    }

    /**
     *  mvc底层数据传输原则
     *  url: http://localhost:8090/user
     *  ModelAndView:
     *      1.model 封装数据的
     *      2.View  封装视图页面的
     */
//    @RequestMapping("/user")
//    public ModelAndView toUser(){
//        ModelAndView modelAndView = new ModelAndView();
//        //封装数据
//        modelAndView.addObject("id", 1001);
//        modelAndView.addObject("name", "安琪拉");
//        //封装页面数据
//        modelAndView.setViewName("user");
//        return modelAndView;
//    }
//    @RequestMapping("/addUser")
    public String addUser(Integer id,String name){
        System.out.println("参数获取"+id+":"+name);
        return "success";
    }


}
