package com.jt.contorller;

import com.jt.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@Controller
@RestController
public class JSONController {
    /**
     * 需求:要求根据getJSON的情求,获取User对象的JSON数据
     * */
    @RequestMapping("/getJSON")
    //@ResponseBody //返回一个JSON串
    public User getJSON(){
        User user = new User();
        user.setId(1000).setName("JSON测试");
        return user;
    }
}
