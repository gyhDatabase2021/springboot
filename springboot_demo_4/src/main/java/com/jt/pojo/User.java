package com.jt.pojo;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;

//pojo实体对象中"必须" 使用包装类型
//规则说明:1.基本类型都有默认值 包装类型默认值为null
//       2.基本类型没有多余的方法 对后续对象代码取值有问题
@Data
@Accessors(chain = true)
public class User {
    private Integer id;
    private String name;
    private String[] hobbys;
    private Dog dog;  //通过对象的引用来赋值
}
